package com.p4.logics.entity;

import java.math.BigDecimal;
import java.util.Date;

public class Transaction{
    // p4 transaction entity
    private int id;
    private Date updatedDate;
    private Integer fileId;
    private Integer txnId;
    private String reference_id;
    private Date date;
    private Date valueDate;
    private String description;
    private String chequeReferenceNo;
    private String transactionType;
    private BigDecimal creditAmount =BigDecimal.ZERO;
    private BigDecimal debitAmount =BigDecimal.ZERO;
    private BigDecimal balanceAmount =BigDecimal.ZERO;
    private String firstLevelClassification;
    private String secondLevelClassification;
    private String thirdLevelClassification;
    private String clientName;
    private String chequeNo;
    private String fiName;
    private Integer balanceError;
    private Integer dateError;
    private String tag;
    private String checkReturnType;
    private Date createdDate;
    private String customerSupplierName;
    private String probableSalary;
    private String logicNO;
    public String getLogicNO() {
        return logicNO;
    }

    public void setLogicNO(String logicNO) {
        this.logicNO = logicNO;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Integer getFileId() {
        return fileId;
    }

    public void setFileId(Integer fileId) {
        this.fileId = fileId;
    }

    public Integer getTxnId() {
        return txnId;
    }

    public void setTxnId(Integer txnId) {
        this.txnId = txnId;
    }

    public String getReference_id() {
        return reference_id;
    }

    public void setReference_id(String reference_id) {
        this.reference_id = reference_id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getValueDate() {
        return valueDate;
    }

    public void setValueDate(Date valueDate) {
        this.valueDate = valueDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getChequeReferenceNo() {
        return chequeReferenceNo;
    }

    public void setChequeReferenceNo(String chequeReferenceNo) {
        this.chequeReferenceNo = chequeReferenceNo;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public BigDecimal getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(BigDecimal creditAmount) {
        this.creditAmount = creditAmount;
    }

    public BigDecimal getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(BigDecimal debitAmount) {
        this.debitAmount = debitAmount;
    }

    public BigDecimal getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(BigDecimal balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getFirstLevelClassification() {
        return firstLevelClassification;
    }

    public void setFirstLevelClassification(String firstLevelClassification) {
        this.firstLevelClassification = firstLevelClassification;
    }

    public String getSecondLevelClassification() {
        return secondLevelClassification;
    }

    public void setSecondLevelClassification(String secondLevelClassification) {
        this.secondLevelClassification = secondLevelClassification;
    }

    public String getThirdLevelClassification() {
        return thirdLevelClassification;
    }

    public void setThirdLevelClassification(String thirdLevelClassification) {
        this.thirdLevelClassification = thirdLevelClassification;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getFiName() {
        return fiName;
    }

    public void setFiName(String fiName) {
        this.fiName = fiName;
    }

    public Integer getBalanceError() {
        return balanceError;
    }

    public void setBalanceError(Integer balanceError) {
        this.balanceError = balanceError;
    }

    public Integer getDateError() {
        return dateError;
    }

    public void setDateError(Integer dateError) {
        this.dateError = dateError;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getCheckReturnType() {
        return checkReturnType;
    }

    public void setCheckReturnType(String checkReturnType) {
        this.checkReturnType = checkReturnType;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCustomerSupplierName() {
        return customerSupplierName;
    }

    public void setCustomerSupplierName(String customerSupplierName) {
        this.customerSupplierName = customerSupplierName;
    }

    public String getProbableSalary() {
        return probableSalary;
    }

    public void setProbableSalary(String probableSalary) {
        this.probableSalary = probableSalary;
    }



//    private int id;
//    private String refId;
//    private String file_id;
//    private Timestamp created_date;
//    private Timestamp updated_date;
//    private String description;
//    private BigDecimal credit_amount=BigDecimal.ZERO;
//    private BigDecimal debit_amount=BigDecimal.ZERO;
//    private BigDecimal balance_amount=BigDecimal.ZERO;
//    private Integer txn_id;
//    private String first_level_classification;
//    private String second_level_classification;
//    private String third_level_classification;
//    private Integer date_error;
//    private Integer balance_error =0;
//    private Date value_date;
//    private Date date;
//    private String cheque_reference_no;
//    private String transaction_type;
//    private String client_name;
//    private String sub_classification;
//    private String sub_classification_type;
//    private String fi_name;
//
//    public boolean isBalanceCreated() {
//        return isBalanceCreated;
//    }
//
//    public void setBalanceCreated(boolean balanceCreated) {
//        isBalanceCreated = balanceCreated;
//    }
//
//    private boolean isBalanceCreated;


}
