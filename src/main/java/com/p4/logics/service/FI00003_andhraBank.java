package com.p4.logics.service;

import com.p4.logics.entity.Transaction;
import org.apache.commons.lang.StringUtils;

import java.util.List;

public class FI00003_andhraBank {
    public List<Transaction> fi00003_andhraBank(List<Transaction> transactionsList){
        //(1) If the initial is "NEFT/" or "RTGS/", then extract data to the right of last "/". Remove numbers, "(" and ")" from the extracted string.
        transactionsList.stream().filter(s->s.getDescription().startsWith("NEFT/")||s.getDescription().startsWith("RTGS/")).forEach(s->s.setCustomerSupplierName(s.getDescription().substring(StringUtils.lastIndexOf(s.getDescription(),"/")).replaceAll("\\d","").replaceAll("[(]","").replaceAll("[)]","")));
        //(2) If the number of "/" in the narration is greater than 1 , then delimit the narration using the "/" as a separater.Once the narration is split in different strings we start from the second string ,if the string is not only numbers and the length of the string is greater than 1 , then consider this else check the next string till the 6th string.If no string satisfies the conditions , then give nil. Similarly if the number of "\" in the narration is greater than 1 , then we delimit the narration using "\" as a separater.And use the same conditions as above.
        transactionsList.stream().filter(s->StringUtils.countMatches(s.getDescription(),"/")>1).forEach(s->{
            String[] splitString = s.getDescription().split("/");
            for (int i=2;i<splitString.length;i++) {
                if(splitString[i].length()>1&&splitString[i].matches("[a-zA-z]+")){
                    s.setCustomerSupplierName(splitString[i]);
                }
            }
        });
        //(3) If the narration begins with "RTGS fm" or "NEFT fm" or "NEFT TO" or "RTGS TO" and the narration contains ":", then extract data between "RTGS fm" or "RTGS TO" or "NEFT fm" or "NEFT TO" and ":".
        transactionsList.stream().filter(s->s.getDescription().startsWith("RTGS fm")||s.getDescription().startsWith("NEFT fm")||s.getDescription().startsWith("NEFT TO")||s.getDescription().startsWith("RTGS TO") && s.getDescription().contains(":")).forEach(s->{
            String extkeyword = null;
            if(s.getDescription().startsWith("RTGS fm")){
                extkeyword =s.getDescription().substring(s.getDescription().indexOf("RTGS fm"),s.getDescription().indexOf(":"));
            }
            if(s.getDescription().startsWith("RTGS TO")){
                extkeyword =s.getDescription().substring(s.getDescription().indexOf("RTGS TO"),s.getDescription().indexOf(":"));
            }
            if(s.getDescription().startsWith("NEFT fm")){
                extkeyword =s.getDescription().substring(s.getDescription().indexOf("NEFT fm"),s.getDescription().indexOf(":"));
            }
            if(s.getDescription().startsWith("NEFT TO")){
                extkeyword =s.getDescription().substring(s.getDescription().indexOf("NEFT TO"),s.getDescription().indexOf(":"));
            }
            s.setCustomerSupplierName(extkeyword);
        });

        //   (4) If initial is "NEFT TO" and the narration contains alphanumeric characters, then extract data after last numeric digit.
        transactionsList.stream().filter(s->s.getDescription().startsWith("NEFT TO") && s.getDescription().matches("[A-Za-z0-9]+")).forEach(s->s.setCustomerSupplierName(s.getDescription().substring(StringUtils.lastIndexOfAny(s.getDescription(),new String[] {"1","2","3","4","5","6","7","8","9","0"}))));

        //(5) If initial is "BR/IMPS-" or "IMPS/", then extract data after last "/" if the string after the last "/" does not have numbers.
        transactionsList.stream().filter(s->s.getDescription().startsWith("BR/IMPS-") || s.getDescription().startsWith("IMPS/")).forEach(s->{
            String extKeyword = s.getDescription().substring(StringUtils.lastIndexOf(s.getDescription(),"/"));
            if(!StringUtils.containsAny(s.getDescription(),new char[] {'1','2','3','4','5','6','7','8','0','9'})){
                s.setCustomerSupplierName(extKeyword);
            }
        });
        //(6) If initial is "IMPS/" and the narration ends with "/", then extract data between 2nd and 3rd "/". Remove numbers from the extracted string. Remove alphanumeric data.
        transactionsList.stream().filter(s->s.getDescription().startsWith("IMPS/")&&s.getDescription().endsWith("/")).forEach(s->{
            String extKeyword =  s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",2),StringUtils.ordinalIndexOf(s.getDescription(),"/",3));
            //alphanumeric logic pending.
            extKeyword.replaceAll("\\d","");
            s.setCustomerSupplierName(extKeyword);
        });
        //   7) If initial is "IMPSRRN:" or "IMPS RRN:" and the count of slash "/" is 1, then extract data after the last "/". Remove numbers and "To " and "F rom" and "From" from the extracted string.
        transactionsList.stream().filter(s->s.getDescription().startsWith("IMPSRRN:")||s.getDescription().startsWith("IMPS RRN:")&&StringUtils.countMatches(s.getDescription(),"/")==1).forEach(s->s.setCustomerSupplierName(s.getDescription().substring(s.getDescription().lastIndexOf("/")).replace("To ","").replace("F rom","").replace("From","")));

        //Online : (8) If the narration begins with "UPI/" or "UPIAR/" then check if there are 6 slash "/" in the narration, then extract data between third and fourth slash "/" and if there are 7 slash "/", then extract data between third and fifth slash "/".
        transactionsList.stream().filter(s->s.getDescription().startsWith("UPI/")||s.getDescription().startsWith("UPIAR/")).forEach(s->{
            String extKeyword = null;
            if(StringUtils.countMatches(s.getDescription(),"/")==6){
                extKeyword = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",3),StringUtils.ordinalIndexOf(s.getDescription(),"/",4));
            }
            if(StringUtils.countMatches(s.getDescription(),"/")==7){
                extKeyword = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",3),StringUtils.ordinalIndexOf(s.getDescription(),"/",5));
            }
            s.setCustomerSupplierName(extKeyword);
        });

        //Cheque and Online : (9) If the narration does not contain any number, then extract entire data. Remove "FROM " or "FR " or "TFR" or "FOR " or "TR FR" or "FRM " or "TO " or "TR FM " or "TFR FROM" or "CH " or "TRANSFERS" or "TR TO" or "TR " or "TRANSFER TO " or "TO " from the beginning of the extracted data. Do not extract if the narration equals "TR".
        transactionsList.stream().filter(s->s.getDescription().matches("[^0-9]*")).forEach(s->s.setCustomerSupplierName(s.getDescription().replace("FROM","").replace("FR","").replace("TFR","").replace("FOR ","").replace("TR FR" ,"").replace( "FRM ","").replace("TO ","").replace( "TR FM ","").replace( "TFR FROM","").replace( "CH ","").replace( "TRANSFERS","").replace("TR TO","").replace("TR ","").replace("TO ","")
        ));

        //Online : (10) If the narration begins with "MOBTRF/", then extract data after second slash "/" till the last character or third slash "/" whichever is earlier.
        transactionsList.stream().filter(s->s.getDescription().startsWith("MOBTRF/")).forEach(s->{
            String extKeyword = null;
            if(StringUtils.ordinalIndexOf(s.getDescription(),"/" ,3)>StringUtils.indexOf(s.getDescription()," ",StringUtils.ordinalIndexOf(s.getDescription(),"/" ,2))){
                extKeyword =  s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/" ,2),StringUtils.indexOf(s.getDescription()," ",StringUtils.ordinalIndexOf(s.getDescription(),"/" ,2)));
            }
            else {
                extKeyword =  s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(), "/", 2), StringUtils.ordinalIndexOf(s.getDescription(), "/", 3));
            }
            s.setCustomerSupplierName(extKeyword);
        });

        //Online : (11) If the narration begins with "POSTXN/", then extract data between first and second slash "/". Remove "RUPAY" from the extracted data.
        transactionsList.stream().filter(s->s.getDescription().startsWith("POSTXN/")).forEach(s->s.setCustomerSupplierName(s.getDescription().substring(s.getDescription().indexOf("/"),StringUtils.ordinalIndexOf(s.getDescription(),"/",2)).replace("RUPAY","")));

        //Online : (12) If the narration begins with "MOBFT to: ", then extract data between colon ":" and slash "/".
        transactionsList.stream().filter(s->s.getDescription().startsWith("MOBFT to:")).forEach(s->s.setCustomerSupplierName(s.getDescription().substring(s.getDescription().indexOf(":"),s.getDescription().indexOf("/"))));

        //Cheque : (13) If the narrations ends with " AB CTS CLG" or "SERVICE CLG" or " TRFR" or "AB CTS", then extract data to the left of numbers.
        transactionsList.stream().filter(s->s.getDescription().endsWith("AB CTS CLG")||s.getDescription().endsWith("SERVICE CLG")||s.getDescription().endsWith("TRFR")||s.getDescription().endsWith("AB CTS")).forEach(s->StringUtils.substringAfter(s.getDescription(),"a"));//seperator logic pendig
        //Others and Cheque : (14) If the narration begins with numbers of length 15, then extract data to the right of numbers.
        transactionsList.stream().filter(s->s.getDescription().matches("\\d{15}.*")).forEach(s->s.setCustomerSupplierName(s.getDescription().replaceAll("\\d{15}"," ")));
        //Cheque : (15) If the narration ends with numbers of length 15 (removing spaces), then extract data to the right of numbers. The length of the extracted string should be more than 5. Remove numbers and special characters from the extracted data.
        transactionsList.stream().filter(s->s.getDescription().matches(".*\\d{15}")).forEach(s->{
            String extKeyword = s.getDescription().substring(StringUtils.indexOfAny(s.getDescription(),new char[] {'1','2','3','4','5','6','7','8','0','9'} ));
            if(extKeyword.length()>5){
                extKeyword.replaceAll("[^a-zA-Z]","");
                s.setCustomerSupplierName(extKeyword);
            }
        });
// ECS/NACH : (16) If the narration begins with "NACH -" or "NACH-", then extract data between "-" and slash "/".
        transactionsList.stream().filter(s->s.getDescription().startsWith("NACH -")||s.getDescription().startsWith("NACH-")).forEach(s->s.setCustomerSupplierName(s.getDescription().substring(s.getDescription().indexOf("-"),s.getDescription().indexOf("/"))));

//  ECS/NACH : (17) If the narration begins with "ACH-", then extract data between "-" second and third hyphen "-".
        transactionsList.stream().filter(s->s.getDescription().startsWith("ACH-")).forEach(s->s.setCustomerSupplierName(s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"-",2),StringUtils.ordinalIndexOf(s.getDescription(),"-",3))));
        // Tax, Loan, Insurance and Investment : (18) If the narration begins with "RTGS/" or "NEFT/" followed by alphanumeric data, then extract data after second slash "/". Remove "FUND TRU" from the extracted data.
        transactionsList.stream().filter(s->s.getDescription().matches("(RTGS/)[a-zA-Z0-9]*")||s.getDescription().matches("(NEFT/)/[a-zA-Z0-9]*")).forEach(s->s.setCustomerSupplierName(s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",2)).replace("FUND TRU" ,"")));
// Loan, Insurance and Investment : (19) If the narration does not contain any number, then extract entire narration. Remove "TR TO " or "TR " or "FOR " or "TRF TO " or "Y/S " or "TRF " or  "TO TR " or " TRF" or "TRFD TO " or "TYRF TO " from the extracted data.
        transactionsList.stream().filter(s->s.getDescription().matches("[^0-9]*")).forEach(s->s.setCustomerSupplierName(s.getDescription().replace("TR TO","").replace("TR","").replace("FOR ","").replace("TRF TO","").replace("Y/S","").replace("TRF","").replace("TO TR","").replace("TRF","").replace("TRFD TO","").replace("TYRF TO","")));
// Investment : (20) If the narration ends with numbers of length 6-20, then extract data to the left of numbers. Remove "TFR TO " or "Y/S " or "TRANSFER TO" or  "TOWARDS " or "TR TO " from the extracted data.
        transactionsList.stream().filter(s->s.getDescription().matches(".*\\d{6,20}")).forEach(s->s.setCustomerSupplierName(s.getDescription().substring(0,StringUtils.indexOfAny(s.getDescription(),new char[] {'1','2','3','4','5','6','7','8','0','9'} )).replace("TFR TO ","").replace("Y/S ","").replace("TRANSFER TO","").replace("TOWARDS","").replace("TR TO","")));
// Insurance : (21) If the narration begins with "IB/", then extend data after last slash "/". Remove numbers from the extracted data.
        transactionsList.stream().filter(s->s.getDescription().startsWith("IB/")).forEach(s->s.setCustomerSupplierName(s.getDescription().substring(s.getDescription().lastIndexOf("/")).replaceAll("[0-9]","")));
// Loan : (22) If the narration begins with "IB/", then extract data between second and third slash "/".
        transactionsList.stream().filter(s->s.getDescription().startsWith("IB/")).forEach(s->s.setCustomerSupplierName(s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",2),StringUtils.ordinalIndexOf(s.getDescription(),"/",3))));
// Tax : (23) If the narration begins with "IB/", then extract data between first and second slash "/".
        transactionsList.stream().filter(s->s.getDescription().startsWith("IB/")).forEach(s->s.setCustomerSupplierName(s.getDescription().substring(s.getDescription().indexOf("/"),StringUtils.ordinalIndexOf(s.getDescription(),"/",2))));
// Tax : (24) If the narration begins with "NEFT/" and the narration does not contain any number, then extract data after first slash "/".
        transactionsList.stream().filter(s->s.getDescription().startsWith("NEFT/") && !s.getDescription().matches(".*\\d.*")).forEach(s->s.setCustomerSupplierName(s.getDescription().substring(s.getDescription().indexOf("/"))));
        return transactionsList;
    }
}
