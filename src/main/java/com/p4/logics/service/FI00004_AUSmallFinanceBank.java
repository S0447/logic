package com.p4.logics.service;

import com.p4.logics.entity.Transaction;
import org.apache.commons.lang.StringUtils;

import java.util.List;

public class FI00004_AUSmallFinanceBank {
    public List<Transaction> fI00004_AUSmallFinanceBank(List<Transaction> transactionsList){

        //logic 1 If the narration has initials as "NEFT" or "RTGS " or "IMPS-",then extract data between third "-" and second "-". Do not extract If the extracted string equals numbers or contains "Bank" or if the length of the extracted string is less than 5.

        //logic 2 Else If the narration has initials as "RTGS-", then extract data from the right of second "-"
        transactionsList.stream().filter(s->s.getDescription().startsWith("RTGS-")).forEach(s-> {
            String exkeyword = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(), "-", 2) + 1);
            s.setCustomerSupplierName(exkeyword);

        });



        //logic 3 If the narratin begins with "UPI/", then extract data to the right of "@ybl/".
        transactionsList.stream().filter(s->s.getDescription().startsWith("UPI/")).forEach(s-> {
            String exkeyword = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(), "@ybl/", 1) + 1);
            s.setCustomerSupplierName(exkeyword);

        });


        //logic 4 If the narration begins with "FT -", extract data between first and second hyphen "-"
        transactionsList.stream().filter(s->s.getDescription().startsWith("FT -")).forEach(s-> {
            String exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"-",1),StringUtils.ordinalIndexOf(s.getDescription(),"-",2));
            s.setCustomerSupplierName(exKeyWord);
        });



        //logic 5 If the narration begins with "FT - Cr -" or "NDA-" or "VDP-" or "VDE-", then extract data after last hypen "-".
        transactionsList.stream().filter(s->s.getDescription().startsWith("FT - Cr -")||s.getDescription().startsWith("NDA-")||s.getDescription().startsWith("VDP-")||s.getDescription().startsWith("VDE-")).forEach(s->{
            String exkeyword = s.getDescription().substring(s.getDescription().lastIndexOf("-"));
            s.setCustomerSupplierName(exkeyword);
        });



        //logic 6 If the narration begins with "UPI/DR" or "UPI/CR" and the narration contains total 3 slash "/", then extract data between third slash "/" and hyphen "-".




        //logic 7 If the narration begins with "UPI/DR" or "UPI/CR", then extract data after third and fourth or fourth and fifth slash "/". The length of the extracted string should be of more than 5 characters. Remove numbers from the extracted data.





        //logic 8 If the narration contains total 4 slash, then extract data to the right of last slash "/". The length of the extracted string should be of more than 5 characters. Remove numbers from the extracted data.





        //logic 9 If the narration begins with "BY CLG ", then extract data between numbers or "BY CLG " (which ever is latter) and hyphen "-" or last space (which ever comes first).





        //logic 10 If the narration begins with "CHQ PAID-" or "CHQ PAID ", then extract data after "CHQ PAID-" or "CHQ PAID ".





        //logic 11 If the narration contains "-CHQ PAID-" or "- CHQ PAID-" or "-CHQ PAID -", then extract data to the right of "-CHQ PAID-" or "- CHQ PAID-" or "-CHQ PAID -".
       /* transactionsList.stream().filter(s->s.getDescription().contains("-CHQ PAID-")||s.getDescription().contains("- CHQ PAID-")||s.getDescription().contains("-CHQ PAID -")).forEach(s->{

            if(s.getDescription().contains("-CHQ PAID-")) {
               s.setCustomerSupplierName(s.getDescription().substring(s.getDescription().indexOf("-CHQ PAID-")));
            }
            if(s.getDescription().contains("- CHQ PAID-")) {
                s.setCustomerSupplierName(s.getDescription().substring(s.getDescription().indexOf("- CHQ PAID-")));
            }
            if(s.getDescription().contains("-CHQ PAID -")) {
                s.setCustomerSupplierName(s.getDescription().substring(s.getDescription().indexOf("-CHQ PAID -")));
            }
            s.setCustomerSupplierName(exKeyWord);
        });*/


        //logic 12 If the narration begins with "FT - DR -" or "FT - CR -" or "FT -", then extract data after last hyphen.
        transactionsList.stream().filter(s->s.getDescription().startsWith("FT - DR -")||s.getDescription().startsWith("FT -")).forEach(s->{
            String exkeyword = s.getDescription().substring(s.getDescription().lastIndexOf("-"));
            s.setCustomerSupplierName(exkeyword);
        });



        //logic 13 If the narration begins with "CASH DEP ", then extract data after "CASH DEP ".
        transactionsList.stream().filter(s->s.getDescription().startsWith("CASH DEP ")).forEach(s->{
            String exKeyWord = s.getDescription().substring(s.getDescription().indexOf("CHEQUE WDL"));
            s.setCustomerSupplierName(exKeyWord);
        });

        //logic 14 If the narration begins with "CHQ PAID" or "ATW-", then extract data after last hyphen.
        transactionsList.stream().filter(s->s.getDescription().startsWith("CHQ PAID")||s.getDescription().startsWith("ATW-")).forEach(s->{
            String exkeyword = s.getDescription().substring(s.getDescription().lastIndexOf("-"));
            s.setCustomerSupplierName(exkeyword);
        });

        //logic 15 If the narration end with numbers preceeded by hyphen "-", then extract data before hyphen "-".


        //logic 16 If the narration begins with "NEFT" then extract data between second and third hyphen "-".
        transactionsList.stream().filter(s->s.getDescription().startsWith("NEFT")).forEach(s->{
            String exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"-",2),StringUtils.ordinalIndexOf(s.getDescription(),"-",3));
            s.setCustomerSupplierName(exKeyWord);
        });

        //logic 17 If the narration contains only numbers between two hyphens "-", then extract data after last hyphen.


        //logic 18 If the narration begins contains "CHQ PAID", then extract data before first hyphen "-".
        transactionsList.stream().filter(s->s.getDescription().startsWith("NEFT")).forEach(s->{
            String exKeyWord = s.getDescription().substring(s.getDescription().indexOf("-"));
            s.setCustomerSupplierName(exKeyWord);
        });








        return transactionsList;
    }

}
