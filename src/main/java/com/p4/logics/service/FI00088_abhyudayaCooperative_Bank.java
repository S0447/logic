package com.p4.logics.service;

import com.p4.logics.entity.Transaction;
import com.p4.logics.validation.ValidationResponse;
import org.apache.commons.lang.StringUtils;

import java.util.List;

public class FI00088_abhyudayaCooperative_Bank {

    public List<Transaction> fI00088_abhyudayaCooperative_Bank(List<Transaction> transactionsList){
//1  If the narration contains ":RTGS:", then extract data between second and third ":". Remove numbers from the extracted string
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("RR") ||s.getSecondLevelClassification() .equalsIgnoreCase("RP") )&&s.getDescription().contains(":RTGS:")).forEach((s)->{String exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),":",2)+1,StringUtils.ordinalIndexOf(s.getDescription(),":",3)).replaceAll("[^A-Za-z ]","");s.setCustomerSupplierName(exKeyWord);s.setLogicNO("1");System.out.println("logic1 "+s.getDescription()+"  ExtKeyword  : "+s.getCustomerSupplierName()+"   2Lvl    "+s.getSecondLevelClassification());});
//2 If the narration contains "NEFT" or "RTGS" and if the narrtions also contains any numeric data or a string "ABHY" or "HDFC",then extract data between "NEFT"or "RTGS" and to the first digit of a numeric data or a first string "ABHY" or "HDFC".Remove the special characters from the extracted data.
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("RR")||s.getSecondLevelClassification().equalsIgnoreCase("RP")||s.getSecondLevelClassification().equalsIgnoreCase("NR")||s.getSecondLevelClassification().equalsIgnoreCase("NP") )&& (s.getDescription().startsWith("NEFT")|| s.getDescription().startsWith("RTGS")) && (s.getDescription().matches(".*\\d*.*")|| s.getDescription().contains("ABHY") || s.getDescription().contains("HDFC"))).forEach(s->{String extKeyWord = " ";
            System.out.println("pnkj "+s.getDescription());
            if(s.getDescription().contains("ABHY")){
                if(s.getDescription().contains("NEFT")){ extKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"NEFT",1)+4,s.getDescription().indexOf("ABHY"));}
                if(s.getDescription().contains("RTGS")){extKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"RTGS",1)+4,s.getDescription().indexOf("ABHY"));}
            }
            if(s.getDescription().contains("HDFC")){
                if(s.getDescription().contains("NEFT")){ extKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"NEFT",1)+4,s.getDescription().indexOf("HDFC"));}
                if(s.getDescription().contains("RTGS")){extKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"RTGS",1)+4,s.getDescription().indexOf("HDFC"));}
            }else {
                if (s.getDescription().contains("NEFT")) {
                    if (StringUtils.indexOfAny(s.getDescription(), new char[]{'1', '2', '3', '4', '5', '6', '7', '8', '0', '9'})<StringUtils.ordinalIndexOf(s.getDescription(), "NEFT", 1)){
                        extKeyWord = s.getDescription().substring(StringUtils.indexOfAny(s.getDescription(), new char[]{'1', '2', '3', '4', '5', '6', '7', '8', '0', '9'}),StringUtils.ordinalIndexOf(s.getDescription(), "NEFT", 1) + 4);
                    }else {
                        extKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(), "NEFT", 1) + 4, StringUtils.indexOfAny(s.getDescription(), new char[]{'1', '2', '3', '4', '5', '6', '7', '8', '0', '9'}));
                    }
                }
                if (s.getDescription().contains("RTGS")) {
                    if (StringUtils.indexOfAny(s.getDescription(), new char[]{'1', '2', '3', '4', '5', '6', '7', '8', '0', '9'})<StringUtils.ordinalIndexOf(s.getDescription(), "RTGS", 1)){
                        extKeyWord = s.getDescription().substring(StringUtils.indexOfAny(s.getDescription(), new char[]{'1', '2', '3', '4', '5', '6', '7', '8', '0', '9'}),StringUtils.ordinalIndexOf(s.getDescription(), "RTGS", 1) + 4);
                    }else {
                        extKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(), "RTGS", 1) + 4, StringUtils.indexOfAny(s.getDescription(), new char[]{'1', '2', '3', '4', '5', '6', '7', '8', '0', '9'}));
                    }
                }
            }
            extKeyWord = extKeyWord.replaceAll("[^A-Z a-z ]","");
            s.setCustomerSupplierName(extKeyWord);
            s.setLogicNO("2");
            System.out.println("logic2 "+s.getDescription()+"  ExtKeyword  : "+s.getCustomerSupplierName()+"   2Lvl    "+s.getSecondLevelClassification());
        });
//Updated logic 3,4
        transactionsList.stream().filter(s ->(s.getSecondLevelClassification().equalsIgnoreCase("RR")||s.getSecondLevelClassification().equalsIgnoreCase("RP")|| s.getSecondLevelClassification().equalsIgnoreCase("NP")||s.getSecondLevelClassification().equalsIgnoreCase("NR") &&  s.getDescription().startsWith("OrigBrCd"))&& s.getDescription().contains("neft")||s.getDescription().contains("rtgs")).forEach((s) -> {
            String exKeyWord = s.getDescription().substring(s.getDescription().indexOf("=") + 1, s.getDescription().indexOf("NEFT"));
            exKeyWord=exKeyWord.replaceAll("rtgs","");
            if(exKeyWord.contains("[0-9]")||exKeyWord.contains("[0-9]")&&exKeyWord.contains("ABB TR (ONLINE)")){
                exKeyWord = s.getDescription().substring(s.getDescription().indexOf("neft"));
                exKeyWord = s.getDescription().substring(s.getDescription().indexOf("rtgs"));
            }
            else{
                exKeyWord = s.getDescription().substring(s.getDescription().indexOf("=")+1,s.getDescription().indexOf("neft")).replaceAll("DR-","").replaceAll("\\\\W+","");
            }
            if(exKeyWord.length()>=4) {
                s.setCustomerSupplierName(exKeyWord);
            }
            s.setLogicNO("3-4");
            System.out.println("logic3,4 "+s.getDescription()+"  ExtKeyword  : "+s.getCustomerSupplierName()+"   2Lvl    "+s.getSecondLevelClassification());
        });

//5 If the narration contains only alphabets and no numbers and no special characters (Except ".") and does not contain "Bank" or "ZZZZZZZZZZZZZZZZ", then extract that entire string.
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("OP")||s.getSecondLevelClassification().equalsIgnoreCase("OR"))&& s.getDescription().matches("[A-Za-z.]+") && !s.getDescription().contains("Bank") && !s.getDescription().contains("ZZZZZZZZZZZZZZZZ")).forEach(s->{s.setCustomerSupplierName(s.getDescription());s.setLogicNO("5");System.out.println("logic5 "+s.getDescription()+"  ExtKeyword  : "+s.getCustomerSupplierName()+"   2Lvl    "+s.getSecondLevelClassification());});
//6 If the narrations begins with "RTGS:", then extract the data between the first ":" and "(". Remove numbers and special characters from the extracted string.
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("RR")||s.getSecondLevelClassification().equalsIgnoreCase("RP")) && (s.getDescription().startsWith("RTGS:")&&s.getDescription().contains("("))).forEach(s->{
            s.setCustomerSupplierName(s.getDescription().substring(s.getDescription().indexOf(":"),s.getDescription().indexOf("(")).replaceAll("[^a-zA-Z ]",""));
            s.setLogicNO("6");
            System.out.println("logic6 "+s.getDescription()+"  ExtKeyword  : "+s.getCustomerSupplierName()+"   2Lvl    "+s.getSecondLevelClassification());

        });
//7 If the narrations begins with "RTGSDR-", then see if the narration contains numbers of length 15, if so, then extract data from the right of numbers. Remove numbers and special characters from the extracted string.
        transactionsList.stream().filter(s ->(s.getSecondLevelClassification().equalsIgnoreCase("RR")||s.getSecondLevelClassification().equalsIgnoreCase("RP")) && s.getDescription().startsWith("RTGSDR-")).forEach(s->{
            if(s.getDescription().matches(".*\\d{15}.*")){  //can also use "[^0-9]*\d{15}[^0-9]*"
                String extKeyword =  s.getDescription().substring(StringUtils.lastIndexOfAny(s.getDescription(),new String[] {"1","2","3","4","5","6","7","8","9","0"}));
                extKeyword = extKeyword.replaceAll("[^a-zA-Z]","");
                s.setCustomerSupplierName(extKeyword);
                s.setLogicNO("7");
                System.out.println("logic7 "+s.getDescription()+"  ExtKeyword  : "+s.getCustomerSupplierName()+"   2Lvl    "+s.getSecondLevelClassification());
            }
        });

// logic 8 Check if the narration begins with "OrigBrCd = " followed by numbers of length 2, then extract data between "OrigBrCd = " and "SB/". The length of the extracted character should be more than 5 after removing numbers and special characters.
        transactionsList.stream().filter(s ->(s.getSecondLevelClassification().equalsIgnoreCase("OP")||s.getSecondLevelClassification().equalsIgnoreCase("OR")) && s.getDescription().startsWith("OrigBrCd =")).forEach((s) -> {

            if (s.getDescription().matches("[OrigBrCd =]+[0-9]{2}")) {
                String exKeyWord = s.getDescription().substring(s.getDescription().indexOf("OrigBrCd ="), s.getDescription().indexOf("SB/"));
                exKeyWord = exKeyWord.replaceAll("[^A-Za-z ]",  "");
                if (exKeyWord.length() > 5) {
                    s.setCustomerSupplierName(exKeyWord);
                }
            }
            s.setLogicNO("8");
            System.out.println("logic8 "+s.getDescription()+"  ExtKeyword  : "+s.getCustomerSupplierName()+"   2Lvl    "+s.getSecondLevelClassification());
        });
//(9) Updated Logic Check if the narration begins with "OrigBrCd = " followed by numbers of length 2 and does not contains "SB/", then extract data to the right of the same only if it does not contain numbers and special characters.
        transactionsList.stream().filter(s ->(s.getSecondLevelClassification().equalsIgnoreCase("OP")||s.getSecondLevelClassification().equalsIgnoreCase("OR"))&& s.getDescription().startsWith("OrigBrCd =")).forEach((s) -> {
            if (s.getDescription().contains("[OrigBrCd =]+[0-9]{2}")&& !s.getDescription().contains("SB/")) {
                String exkeyword = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(), "OrigBrCd =", 1), s.getDescription().length() - 1);
                if (!exkeyword.contains("[^a-zA-Z0-9]\", \" ")) {
                    s.setCustomerSupplierName(exkeyword);
                }
            }
            s.setLogicNO("9");
            System.out.println("logic9 "+s.getDescription()+"  ExtKeyword  : "+s.getCustomerSupplierName()+"   2Lvl    "+s.getSecondLevelClassification());
        });
//10Check if the narration begins with numbers "CC/", then extract numbers to the right after the first space.
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("OP")||s.getSecondLevelClassification().equalsIgnoreCase("OR")) &&s.getDescription().startsWith("CC/")).forEach(s->{s.setCustomerSupplierName(s.getDescription().substring(s.getDescription().indexOf(" ")));System.out.println("logic10 "+s.getDescription()+"  ExtKeyword  : "+s.getCustomerSupplierName()+"   2Lvl    "+s.getSecondLevelClassification());});
// 13 If the narration begins with "To:RUPAY/POS/" or "To:RUPAY/ECOM/", then extract data between 2nd and 3rd slash "/".
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("OP")||s.getSecondLevelClassification().equalsIgnoreCase("OR") )&& s.getDescription().startsWith("To:RUPAY/POS/")||s.getDescription().startsWith("To:RUPAY/ECOM/")).forEach(s->{s.setCustomerSupplierName(s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",2)+1,StringUtils.ordinalIndexOf(s.getDescription(),"/",3)));System.out.println("logic13 "+s.getDescription()+"  ExtKeyword  : "+s.getCustomerSupplierName()+"   2Lvl    "+s.getSecondLevelClassification());});
//14 If the narration begins with "CTS" then extract data after CTS.Remove "-" from the beginning of the extracted string. The length of the extracted string should not be less than 3
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("CP")||s.getSecondLevelClassification().equalsIgnoreCase("CR") )&& s.getDescription().startsWith("CTS")).forEach(s->{
            String extKeyword = s.getDescription().substring(StringUtils.indexOf(s.getDescription(), "CTS")+3).replace("-", "");
            if (extKeyword.length()>3){
                s.setCustomerSupplierName(extKeyword);
            }
            s.setLogicNO("14");
            System.out.println("logic14 "+s.getDescription()+"  ExtKeyword  : "+s.getCustomerSupplierName()+"   2Lvl    "+s.getSecondLevelClassification());
            ;});
// 15 If the narration does not contain any number or just one number, then extract entire narration and remove numbers. The length of the extracted data should not be less than 6.
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("CP") || s.getSecondLevelClassification().equalsIgnoreCase("CP")) && s.getDescription().matches(".*\\d?.*")).forEach(s->{
            String extKeyword = s.getDescription();
            extKeyword = extKeyword.replaceAll("[0-9]","");
            if (extKeyword.length()>=6){
                s.setCustomerSupplierName(extKeyword);
            }
            s.setLogicNO("15");
            System.out.println("logic15 "+s.getDescription()+"  ExtKeyword  : "+s.getCustomerSupplierName()+"   2Lvl    "+s.getSecondLevelClassification());
        });
//16 Updated logic If the narration begins with "SB/", then extract data after last number. Remove "ABB TR (ONLINE)" and special characters from the extracted data.
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("CP")||s.getSecondLevelClassification().equalsIgnoreCase("CR"))&& s.getDescription().startsWith("SB/")).forEach(s->{s.setCustomerSupplierName(s.getDescription().substring(StringUtils.lastIndexOfAny(s.getDescription(),new String[] {"1","2","3","4","5","6","7","8","9","0"})).replaceAll("ABB TR (ONLINE)","").replaceAll("[^A-Za-z 0-9]","").replaceAll("[0-9]",""));
            s.setLogicNO("16");
            System.out.println("logic16 "+s.getDescription()+"  ExtKeyword  : "+s.getCustomerSupplierName()+"   2Lvl    "+s.getSecondLevelClassification());
        });
//17 If the narration begins with "OrigBrCd =", then extract data between the end of first numeric digit and "SB/" or " SB " or " SB-" or " CD " or " CD/". Remove numbers and special characters and "ABB TR (ONLINE)".
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("CP")||s.getSecondLevelClassification().equalsIgnoreCase("CR")||s.getSecondLevelClassification().equalsIgnoreCase("OPay")||s.getSecondLevelClassification().equalsIgnoreCase("ORec"))&&(s.getDescription().startsWith("OrigBrCd ="))&&(s.getDescription().contains("SB/")||s.getDescription().contains("SB")||s.getDescription().contains("SB-")||s.getDescription().contains("CD")||s.getDescription().contains("CD/"))).forEach(s->{
            String extKeyword = null;
            if (s.getDescription().contains("SB/")){
                extKeyword =s.getDescription().substring(StringUtils.indexOfAny(s.getDescription(),new String[] {"1","2","3","4","5","6","7","8","9","0"})+2,s.getDescription().indexOf("SB/"));
            }
            if (s.getDescription().contains("SB")){
                if (StringUtils.indexOfAny(s.getDescription(),new String[] {"1","2","3","4","5","6","7","8","9","0"})>s.getDescription().indexOf("SB")){
                    extKeyword = s.getDescription().substring(StringUtils.lastIndexOfAny(s.getDescription(),new String[] {"1","2","3","4","5","6","7","8","9","0"}));
                }else {
                    extKeyword = s.getDescription().substring(StringUtils.indexOfAny(s.getDescription(), new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "0"}) + 2, s.getDescription().indexOf("SB"));
                }
            }
            if (s.getDescription().contains("SB-")){
                extKeyword =s.getDescription().substring(StringUtils.indexOfAny(s.getDescription(),new String[] {"1","2","3","4","5","6","7","8","9","0"})+2,s.getDescription().indexOf("SB-"));
            }
            if (s.getDescription().contains("CD")){
                extKeyword =s.getDescription().substring(StringUtils.indexOfAny(s.getDescription(),new String[] {"1","2","3","4","5","6","7","8","9","0"})+2,s.getDescription().indexOf("CD"));
            }
            if (s.getDescription().contains("CD/")){
                extKeyword =s.getDescription().substring(StringUtils.indexOfAny(s.getDescription(),new String[] {"1","2","3","4","5","6","7","8","9","0"})+2,s.getDescription().indexOf("CD/"));
            }
            extKeyword = extKeyword.replace("ABB TR (ONLINE) ","").replaceAll("[^a-zA-Z ]","");
            s.setCustomerSupplierName(extKeyword);
            s.setLogicNO("17");
            System.out.println("logic17 "+s.getDescription()+"  ExtKeyword  : "+s.getCustomerSupplierName()+"   2Lvl    "+s.getSecondLevelClassification());
        });

// 18 If the narration begins with "S.I No", then extract data after last slash "/". Tax and Insurance :
        transactionsList.stream().filter(s ->(s.getSecondLevelClassification().equalsIgnoreCase("TP")||s.getSecondLevelClassification().equalsIgnoreCase("Trefund")||s.getSecondLevelClassification().equalsIgnoreCase("InsPP")||s.getSecondLevelClassification().equalsIgnoreCase("InsPR")) && s.getDescription().startsWith("S.I No")).forEach((s) -> {
            if (s.getDescription().contains("S.I No")) {
                String exkeyword = s.getDescription().substring(s.getDescription().lastIndexOf("/"));
                s.setCustomerSupplierName(exkeyword);
            }
            s.setLogicNO("18");
            System.out.println("logic18 "+s.getDescription()+"  ExtKeyword  : "+s.getCustomerSupplierName()+"   2Lvl    "+s.getSecondLevelClassification());
        });


//logic 19 If the narration does not contain any number, then extract entire data.  Insurance :
        transactionsList.stream().filter(s -> (s.getSecondLevelClassification().equalsIgnoreCase("TP") || s.getSecondLevelClassification().equalsIgnoreCase("Trefund") || s.getSecondLevelClassification().equalsIgnoreCase("InsPP") || s.getSecondLevelClassification().equalsIgnoreCase("InsPR"))&& s.getDescription().matches("[^0-9]*")).forEach((s) -> {
            s.setCustomerSupplierName(s.getDescription());
            s.setLogicNO("19");
            System.out.println("logic19 "+s.getDescription()+"  ExtKeyword  : "+s.getCustomerSupplierName()+"   2Lvl    "+s.getSecondLevelClassification());
        });

//20 If the narration begins with "NEFT", then extract data between "NEFT" and number or alphanumeric data whichever it finds first. Tax :
        transactionsList.stream().filter(s ->(s.getSecondLevelClassification().equalsIgnoreCase("InsPP")||s.getSecondLevelClassification().equalsIgnoreCase("InsPR"))&&s.getDescription().startsWith("NEFT")).forEach((s) -> {
            if (s.getDescription().matches(".*\\d.*")) {
                String exKeyWord = s.getDescription().substring(s.getDescription().indexOf("NEFT")+4,StringUtils.indexOfAny(s.getDescription(),new char[] {'1','2','3','4','5','6','7','8','0','9'} ));
                if (exKeyWord.length() > 5) {
                    exKeyWord =exKeyWord.replace("ABHY","");
                    s.setCustomerSupplierName(exKeyWord);
                }
            }

       /*if (s.getDescription().matches("^[a-zA-Z0-9]+$")) {
           String exKeyWord = s.getDescription().substring(s.getDescription().indexOf("NEFT"),StringUtils.indexOfAny(s.getDescription(),new char[] {'1','2','3','4','5','6','7','8','0','9'} ));
           if (exKeyWord.length() > 5) {
               s.setCustomerSupplierName(exKeyWord);
           }
       }*/
            s.setLogicNO("20");
            System.out.println("logic20 "+s.getDescription()+"  ExtKeyword  : "+s.getCustomerSupplierName()+"   2Lvl    "+s.getSecondLevelClassification());
        });
// Updated logic 21 If the narration begins with "NEFT~", then extract data to the right of "~". Remove numbers, alphanumeric value.
        transactionsList.stream().filter(s ->(s.getSecondLevelClassification().equalsIgnoreCase("TP")||s.getSecondLevelClassification().equalsIgnoreCase("Trefund"))&& s.getDescription().startsWith("NEFT~")) .forEach((s) -> {
            if (s.getDescription().contains("NEFT~")) {
                String exkeyword = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(), "~", 1)+1);
                exkeyword.replaceAll("[0-9]","");
                s.setCustomerSupplierName(exkeyword);
            }
            s.setLogicNO("21");
            System.out.println("logic21 "+s.getDescription()+"  ExtKeyword  : "+s.getCustomerSupplierName()+"   2Lvl    "+s.getSecondLevelClassification());
        });
//Updated logic 22 If the narration begins with "QIC/", then extract data after ":" and third "/". Remove "CloseQICRIP"
        transactionsList.stream().filter(s ->(s.getSecondLevelClassification().equalsIgnoreCase("INVESTP")||s.getSecondLevelClassification().equalsIgnoreCase("INVESTR"))&& s.getDescription().startsWith("QIC/")).forEach((s) -> {
            if (s.getDescription().contains("QIC/")) {
                String exKeyWord="";
                exKeyWord = s.getDescription().substring(s.getDescription().indexOf(":")+1,StringUtils.ordinalIndexOf(s.getDescription(), "/", 3)).replaceAll("CloseQICRIP","");
                s.setCustomerSupplierName(exKeyWord);
            }
            s.setLogicNO("22");
            System.out.println("logic22 "+s.getDescription()+"  ExtKeyword  : "+s.getCustomerSupplierName()+"   2lvl :"+s.getSecondLevelClassification());
        });
        return transactionsList;
    }

}
