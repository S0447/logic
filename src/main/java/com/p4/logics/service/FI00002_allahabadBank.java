package com.p4.logics.service;

import com.p4.logics.entity.Transaction;
import org.apache.commons.lang.StringUtils;

import java.util.List;

public class FI00002_allahabadBank {
    public List<Transaction> fI00002_allahabad_Bank(List<Transaction> transactionList){
        //1 If the narration contains "neft-" or "rtgs-", then extract the data between 2nd and 3rd hyphen.
        transactionList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("INVESTP")||s.getSecondLevelClassification().equalsIgnoreCase("INVESTR")||s.getSecondLevelClassification().equalsIgnoreCase("TP")||s.getSecondLevelClassification().equalsIgnoreCase("Trefund")||s.getSecondLevelClassification().equalsIgnoreCase("InsPP")||s.getSecondLevelClassification().equalsIgnoreCase("InsPR")||s.getSecondLevelClassification().equalsIgnoreCase(""))&& s.getDescription().contains("neft-")||s.getDescription().contains("rtgs-")).forEach(s-> {
            String exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"-",2),StringUtils.ordinalIndexOf(s.getDescription(),"-",3));
            s.setCustomerSupplierName(exKeyWord);
            s.setLogicNO("1");
        });

        // 2 if the narration contains "imps-" or "/imps/ or "imps:ift" , then extract the data after "imps-" or "/imps/ or "imps:ift" respectively. Remove numbers, alphanumeric values, "-", "/" and alphabetes between two "-" whose length is equal to or less than 4. length of extracted data should be more than 3.
        transactionList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("INVESTP")||s.getSecondLevelClassification().equalsIgnoreCase("INVESTR")||s.getSecondLevelClassification().equalsIgnoreCase("TP")||s.getSecondLevelClassification().equalsIgnoreCase("Trefund")||s.getSecondLevelClassification().equalsIgnoreCase("InsPP")||s.getSecondLevelClassification().equalsIgnoreCase("InsPR")||s.getSecondLevelClassification().equalsIgnoreCase(""))&& (s.getDescription().contains("imps-")||s.getDescription().contains("/imps/")||s.getDescription().contains("imps:ift"))) .forEach(s-> {
            if(s.getDescription().contains("imps-")) {
                s.setCustomerSupplierName(s.getDescription().substring(s.getDescription().indexOf("imps-")).replaceAll("[^A-Za-z ]",""));
            }
            if(s.getDescription().contains("/imps/")) {
                s.setCustomerSupplierName(s.getDescription().substring(s.getDescription().indexOf("/imps/")).replaceAll("[^A-Za-z ]",""));
            }
            if(s.getDescription().contains("imps:ift")) {
                s.setCustomerSupplierName(s.getDescription().substring(s.getDescription().indexOf("imps:ift")).replaceAll("[^A-Za-z ]",""));
            }
        });

        //3 If the narration starts with "rtgs inw" then extract the data between 2nd and 4th space.
        transactionList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("INVESTP")||s.getSecondLevelClassification().equalsIgnoreCase("INVESTR")||s.getSecondLevelClassification().equalsIgnoreCase("TP")||s.getSecondLevelClassification().equalsIgnoreCase("Trefund")||s.getSecondLevelClassification().equalsIgnoreCase("InsPP")||s.getSecondLevelClassification().equalsIgnoreCase("InsPR")||s.getSecondLevelClassification().equalsIgnoreCase(""))&& (s.getDescription().startsWith("rtgs inw"))).forEach(s-> {
            String exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription()," ",2),StringUtils.ordinalIndexOf(s.getDescription()," ",4));
            s.setCustomerSupplierName(exKeyWord);
            s.setLogicNO("3");
        });

        //4 if the narration begins with "cash cheque" and contains "paid to", then extract the data right of "paid to". Remove "self","(gen)", numbers from the extracted data.
        transactionList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("CASHRP")||s.getSecondLevelClassification().equalsIgnoreCase("CASHRR"))&& (s.getDescription().startsWith("cash cheque")&& s.getDescription().contains("paid to"))).forEach(s->{s.setCustomerSupplierName(s.getDescription().substring(s.getDescription().indexOf("paid to")));
            String exKeyWord=" ";
            exKeyWord= s.getDescription().replaceAll("self","");
            exKeyWord=  s.getDescription().replaceAll("(gen)","");
            s.setCustomerSupplierName(exKeyWord);
            s.setLogicNO("4");
        });

        //5 if the narration is classified as "cheque" and contains two "~", then extract the data right of second "~".
        // Length of the extracted data should be more than 2. remove "to",numbers,"AS PER", "CUSTOMER", "REQUEST","cancellation","cancelled","LCCCMS POOL ACNONUTI","netweb","payment" from the extracted data. ignore if the narration begins with "by clearing ".
        transactionList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("CP")||s.getSecondLevelClassification().equalsIgnoreCase("CR"))).forEach(s->{
            if(s.getDescription().contains("~")) {
                String exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(), "~", 2));
                if (exKeyWord.length() > 2) {
                    exKeyWord.replaceAll("to","").replaceAll("[0-9]","").replaceAll("AS PER","").replaceAll("CUSTOMER","").replaceAll("cancellation","").replaceAll("cancelled","").replaceAll("LCCCMS POOL ACNONUTI","").replaceAll("netweb","").replaceAll("payment","");
                    if(!s.getDescription().startsWith("by clearing ")) {
                        s.setCustomerSupplierName(exKeyWord);
                    }
                }
            }
            s.setLogicNO("5");

        });


        //6if the narration is classified as "other", then extract the data after 3rd "/". If the extracted data contains "/" then consider the data right of "/".
        transactionList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("OPay")||s.getSecondLevelClassification().equalsIgnoreCase("ORec"))).forEach(s->{s.setCustomerSupplierName(s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",3)));
            String exkeyword = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(), "/", 3)+1);
            s.setCustomerSupplierName(exkeyword);
            s.setLogicNO("6");
        });

        //7 If the narration contains "neft-" or "rtgs-", then extract the data between 2nd and 3rd hyphen. REMOVE "DUTY DRAW BACK AC","REFUNDS","E PAO" FROM THE EXTRACTED DATA.
        transactionList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("ECSP")||s.getSecondLevelClassification().equalsIgnoreCase("ECSR"))&& (s.getDescription().contains("neft-")|| s.getDescription().contains("rtgs-"))).forEach(s->{
            String exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"-",2),StringUtils.ordinalIndexOf(s.getDescription(),"-",3));
            exKeyWord.replaceAll("DUTY DRAW BACK AC","").replaceAll("REFUNDS","").replaceAll("EPAO","");
            s.setCustomerSupplierName(exKeyWord);
            s.setLogicNO("7");
        });
        // 8 IF THE NARRATION CONTAINS "ACH-", THEN EXTRACT THE DATA AFTER "ACH-". REMOVE "TRF TO","TPCF","TP ","ACH ","Credit Through PFMS","TP-" AND NUMBERS. IF THE EXTRACTED DATA CONTAINS "-", THEN CONSIDER THE DATA LEFT OF "-".
        transactionList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("OPay")||s.getSecondLevelClassification().equalsIgnoreCase("ORec"))&&(s.getDescription().contains("ACH-"))).forEach(s->{s.setCustomerSupplierName(s.getDescription().substring(s.getDescription().indexOf("ACH-")));
            String exKeyWord=" ";
            exKeyWord= s.getDescription().replaceAll("TRF TO","").replaceAll("TPCF","").replaceAll("TP","").replaceAll("ACH","").replaceAll("Credit Through PFMS","").replaceAll("TP-","").replaceAll("[0-9]","");
            if(s.getDescription().contains("-")) {
                String exkeyword = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(), "-", 1) + 1);
                s.setCustomerSupplierName(exkeyword);
            }
            s.setLogicNO("8");

        });

        //9 IF THE NARRATION BEGINS WITH "INW CLG " OR "TO TRANSFER ", THEN EXTRACT THE DATA AFTER SECOND "~". REMOVE NUMBERS,"INB:","-", "NA","RALB","QALB","PALB","IMPS","PAYMENT","ach" FROM THE EXTRACTED DATA.
        transactionList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("TP")||s.getSecondLevelClassification().equalsIgnoreCase("Trefund")||(s.getSecondLevelClassification().equalsIgnoreCase("InsPP")||s.getSecondLevelClassification().equalsIgnoreCase("InsPR")))&& (s.getDescription().startsWith("INW CLG")||s.getDescription().startsWith("TO TRANSFER"))).forEach(s-> {
            String exkeyword = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(), "~", 2) + 1).replaceAll("[0-9]","").replaceAll("INB:","").replaceAll("-","").replaceAll("NA","").replaceAll("RALB","").replaceAll("QALB","").replaceAll("PALB","").replaceAll("IMPS","").replaceAll("PAYMENT","").replaceAll("ach","");
            s.setCustomerSupplierName(exkeyword);
            s.setLogicNO("9");
        });

        //10 IF THE NARRATION BEGINS WITH "BY TRANSFER" OR "TO CLG", THEN EXTRACT DATA AFTER SECOND "~".REMOVE ALPHANUMERIC VALUES,NUMBERS,"-",":","TRN" FROM THE EXTRACTED DATA.
        //IF THE EXTRACTED DATA CONTAINS "DEDUCTED" THEN CONSIDER THE DATA LEFT OF "DEDUCTED".IF THE LENGTH OF THE EXTRACTED DATA IS 4, THEN REMOVE FIRST LETTER FROM THE DATA.
        transactionList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("TP")||s.getSecondLevelClassification().equalsIgnoreCase("Trefund"))&&(s.getDescription().startsWith("BY TRANSFER")||s.getDescription().startsWith("TO CLG"))).forEach(s-> {
            String exkeyword = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(), "~", 2) + 1).replaceAll("[^A-Za-z ]","").replaceAll("TRN","");
            if(exkeyword.contains("DEDUCTED")){
                String exkeywprd2left =  exkeyword.substring(0,exkeyword.indexOf("DEDUCTED"));
                if(exkeywprd2left.length()>4){
                    exkeyword =exkeywprd2left.substring(1);
                }
            }
            s.setCustomerSupplierName(exkeyword);
            s.setLogicNO("10");
        });

        // 11 (pending)IF THE NRRATION BEGINS WITH "TO TRF", THEN EXTRACT THE DATA AFTER "TO TRF". IF THE EXTRACTED DATA CONTAINS "-" THEN CONSIDER THE DATA LEFT OF "-".
        transactionList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("InsPP")||s.getSecondLevelClassification().equalsIgnoreCase("InsPR"))&&(s.getDescription().startsWith("TO TRF"))).forEach(s-> {
            String exKeyWord = s.getDescription().substring(s.getDescription().indexOf("TO TRF"));
            if(exKeyWord.contains("-")){
                exKeyWord = s.getDescription().substring(0,s.getDescription().indexOf("-"));
                s.setCustomerSupplierName(exKeyWord);
            }
            s.setLogicNO("11");
        });

        //12 IF THE NARRATION BEGINS WITH "CHEQUE WDL" OR "CHQ TRANSFE", THEN EXTRACT THE DATA AFTER "CHEQUE WDL" OR "CHQ TRNSFE". REMOVE ALPHANUMERIC VALUES
        //Done
        transactionList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("InsPP")||s.getSecondLevelClassification().equalsIgnoreCase("InsPR"))&&(s.getDescription().startsWith("CHEQUE WDL")||s.getDescription().startsWith("CHQ TRANSFE"))).forEach(s->{
            String exKeyWord = s.getDescription().substring(s.getDescription().indexOf("CHEQUE WDL"), s.getDescription().indexOf("CHQ TRNSFE")).replaceAll("[^a-zA-Z0-9]", "");
            s.setCustomerSupplierName(exKeyWord);
        });
        //13 if the narration begins with "dep tfr", then extract the data right of "from". Remove numbers and "inb" .
        transactionList.stream().filter(s->(s.getDescription().startsWith("dep tfr"))).forEach(s->{
            String exkeyword = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(), "from", 1)+1).replaceAll("[0-9]","").replaceAll("inb","");
            s.setCustomerSupplierName(exkeyword);
            s.setLogicNO("13");
        });

        return transactionList;
    }
}
