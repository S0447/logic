package com.p4.logics.service;

import com.p4.logics.entity.Transaction;
import org.apache.commons.lang.StringUtils;

import java.util.List;

public class FI00005_axisBank {

    public List<Transaction> fi00005_axisBank(List<Transaction> transactionsList){
//        (1) If classification is RTGS,NEFT Payment/Receipt: If the number of "/" in the narration is greater than 1 ,
//        then delimit the narration using the "/" as a separater.Once the narration is split in different strings we start from the third string ,
//        if there is no number in the string or the count of number in the extracted string is less than count of alphabets and the length of the string is greater than 3 ,
//        then extract this else keep checking till the 6th string.If no string satisfies these conditions then give blank.
//        Similarly if the number of "-" in the narration is greater than 1 , then we delimit the narration using "-" as a separater.And apply the same set of conditions as above.
//        Remove "ibuser-" from the extracted data. ignore "fund transfer"
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("RR")||s.getSecondLevelClassification().equalsIgnoreCase("RP")||s.getSecondLevelClassification().equalsIgnoreCase("NR")||s.getSecondLevelClassification().equalsIgnoreCase("NP") )&&(StringUtils.countMatches(s.getDescription(),"/")>1||StringUtils.countMatches(s.getDescription(),"-")>1)).forEach(s->{
            String extKeyword = " ";
            if(StringUtils.countMatches(s.getDescription(),"/")>1){
                String[] splitString = s.getDescription().split("/" );
                for (int i =2;i<6;i++){
                    if(splitString[i].matches("[^0-9]")){
                        s.setCustomerSupplierName(extKeyword);
                        break;
                    }
                    String number = splitString[i].replaceAll("[^0-9]","");
                    String alphabet = splitString[i].replaceAll("[^A-Za-z]","");
                    if(number.length()<alphabet.length()){
                        if (splitString[i].length()>3){
                            extKeyword = splitString[i];
                            s.setCustomerSupplierName(extKeyword.replace("ibuser-"," ").replace("fund transfer",""));
                            break;
                        }
                    }
                }
            }
            if(StringUtils.countMatches(s.getDescription(),"-")>1){
                String[] splitString = s.getDescription().split("-");
                for (int i =2;i<6;i++){
                    if(splitString[i].matches("[^0-9]")){
                        s.setCustomerSupplierName(extKeyword);
                        break;
                    }
                    String number = splitString[i].replaceAll("[^0-9]","");
                    String alphabet = splitString[i].replaceAll("[^A-Za-z]","");
                    if(number.length()<alphabet.length()){
                        if (splitString[i].length()>3){
                            extKeyword = splitString[i];
                            s.setCustomerSupplierName(extKeyword);
                            break;
                        }
                    }
                }
            }
            s.setLogicNO("1");
            System.out.println("logic No. 1   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });

//        (2) If classification is ONLINE: If the number of "/" in the narration is greater than 1 , then delimit the narration using the "/" as a separater.Once the narration is split in different strings we start from the second string ,if there is no number in the string or the count of number in the extracted string is less than count of alphabets and the length of the string is greater than 4 , then extract this else keep checking till the 6th string.If no string satisfies these conditions then give blank.
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("OP")||s.getSecondLevelClassification().equalsIgnoreCase("OR"))&& StringUtils.countMatches(s.getDescription(),"/")>1).forEach(s->{
            String extKeyword = " ";
            String[] splitString = s.getDescription().split("/" );
            for (int i =1;i<6;i++){
                if(splitString[i].matches("[^0-9]")){
                    s.setCustomerSupplierName(extKeyword);
                    break;
                }
                String number = splitString[i].replaceAll("[^0-9]","");
                String alphabet = splitString[i].replaceAll("[^A-Za-z]","");
                if(number.length()<alphabet.length()){
                    if (splitString[i].length()>4){
                        extKeyword = splitString[i];
                        s.setCustomerSupplierName(extKeyword);
                        break;
                    }
                }
            }
            s.setLogicNO("2");
            System.out.println("logic No. 2   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });
//        (3)if the classification is ONLINE, if the narration contains only alphabets and only one "/", then extract the data left of "/"
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("OP")||s.getSecondLevelClassification().equalsIgnoreCase("OR"))&&s.getDescription().matches("[A-Za-z ]*[/]?[A-Za-z ]*")).forEach(s->{s.setCustomerSupplierName(s.getDescription().substring(0,s.getDescription().indexOf("/")));
            s.setLogicNO("3");
            System.out.println("logic No. 3   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());});
//        (4)if the classification is ONLINE, narration contains only alphabtes then extract the whole narration. Remove "to" from the extracted data.
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("OP")||s.getSecondLevelClassification().equalsIgnoreCase("OR"))&&s.getDescription().matches("[A-Za-z ]*")).forEach(s->{
            s.setCustomerSupplierName(s.getDescription().replace("to",""));
            s.setLogicNO("4");
            System.out.println("logic No. 4   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });
//        CASH (5)if the narration begins with "brn-" and classified this as "cash", then extract the data right of "brn-". Remove "by","to","cash","staff","yourself","self" from the extracted data. Length should be more than 3. if the narration contains "/", then consider the data left of "/".
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("CWD")||s.getSecondLevelClassification().equalsIgnoreCase("CP"))&&s.getDescription().startsWith("brn-")).forEach(s->{
            String exKeyword = s.getDescription().substring(s.getDescription().indexOf("brn-")).replace("brn-","").replace("brn-","").replace("by","").replace("to","").replace("cash","").replace("staff","").replace("yourself","").replace("self","");
            if(s.getDescription().contains("/")){
                exKeyword = s.getDescription().substring(s.getDescription().indexOf("/"));
            }
            if(exKeyword.length()>3) {
                s.setCustomerSupplierName(exKeyword);
            }
            s.setLogicNO("5");
            System.out.println("logic No. 5   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });
//        CASH (6)if the narration begins with "sak/cash" and classified as "cash", then extract the right of 4th "/". If the extracted data contains "/" then consider the data left of "/". Remove "to", "cash", "by", "self", "cheque", "dep", "deposit" from the extracted data. length of the extracted string should be more than 2.
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("CWD")||s.getSecondLevelClassification().equalsIgnoreCase("CP"))&&s.getDescription().startsWith("sak/cash")).forEach(s->{
            String extkeyword = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",4));
            if(extkeyword.contains("/")){
                extkeyword = extkeyword.substring(s.getDescription().indexOf("/"));
                extkeyword = extkeyword.replace("to","").replace("cash","").replace("by","").replace("self","").replace("cheque","").replace("dep","").replace("deposit","");
                if (extkeyword.length()>2){
                    s.setCustomerSupplierName(extkeyword);
                }

            }
            s.setLogicNO("6");
            System.out.println("logic No. 6   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });
//        CHEQUE (7)if the narration begins with "brn-clg" and classified as "cheque", then extract the data right of "paid to" or "p aid t o". If the extracted data contains "/" then consider the data left of "/".
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("CP")||s.getSecondLevelClassification().equalsIgnoreCase("CR"))&&s.getDescription().startsWith("brn-clg")).forEach(s->{
            String extKeyword = "";
            if(s.getDescription().contains("paid to")){
                extKeyword = s.getDescription().substring(s.getDescription().indexOf("paid to"));
                if(extKeyword.contains("/")){
                    extKeyword = extKeyword.substring(0, transactionsList.indexOf("/"));
                    s.setCustomerSupplierName(extKeyword);
                }
            }
            if(s.getDescription().contains("p aid t o")){
                extKeyword = s.getDescription().substring(s.getDescription().indexOf("paid to"));
                if(extKeyword.contains("/")){
                    extKeyword = extKeyword.substring(0, transactionsList.indexOf("/"));
                    s.setCustomerSupplierName(extKeyword);
                }
            }
            s.setLogicNO("7");
            System.out.println("logic No. 7   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });
//        CHEQUE (8)if the narration is classified as "other" or "online" in first level classification AND "cheque" in the second level, then extract the whole narration if the narration contains only alphabets. Remove "to"
        transactionsList.stream().filter(s->(s.getFirstLevelClassification().equalsIgnoreCase("OPay")||s.getFirstLevelClassification().equalsIgnoreCase("ORec")||s.getFirstLevelClassification().equalsIgnoreCase("OP")||s.getFirstLevelClassification().equalsIgnoreCase("OR"))&&(s.getSecondLevelClassification().equalsIgnoreCase("CP")||s.getSecondLevelClassification().equalsIgnoreCase("CR"))).forEach(s->{
            if(s.getDescription().matches("[^0-9]*")) {
                s.getDescription().replace("to", "");
            }
            s.setLogicNO("8");
            System.out.println("logic No. 8   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });

//        CHEQUE (9)if the narration is classifies as "online" in first level classification and "cheque" in the second level, then extract the data right of 2nd "/". If the extracted data contains "/" then consider data left of "/". Remove "transfer","trf","to","by",numbers,"bill ref no" from the extracted data.

        transactionsList.stream().filter(s->(s.getFirstLevelClassification().equalsIgnoreCase("OPay")||s.getFirstLevelClassification().equalsIgnoreCase("ORec")||(s.getSecondLevelClassification().equalsIgnoreCase("CP")||s.getSecondLevelClassification().equalsIgnoreCase("CR")))).forEach(s->{
            String extKeyword = " ";
            extKeyword = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",2));
            if(extKeyword.contains("/")){
                extKeyword = extKeyword.substring(0,s.getDescription().indexOf("/"));
                extKeyword = extKeyword.replace("transfer","").replace("trf","").replace("to","").replace("by","").replace("bill ref no","").replaceAll("[0-9]","");
                s.setCustomerSupplierName(extKeyword);
            }
            s.setLogicNO("9");
            System.out.println("logic No. 9   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });
//        CHEQUE (10)if the narration is classified as "cheque" and begins with "trf/", then extract the data between 1st and 2nd "/". If the extracted data contain only numbers then extract the data between 2nd and 3rd "/". Length of the extracted data should be more than 3.
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("CP")||s.getSecondLevelClassification().equalsIgnoreCase("CR"))&s.getDescription().startsWith("trf")).forEach(s->{
            String extkeyword = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",2),StringUtils.ordinalIndexOf(s.getDescription(),"/",3));
            if(extkeyword.length()>3){
                s.setCustomerSupplierName(extkeyword);
            }
            s.setLogicNO("10");
            System.out.println("logic No. 10   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });
//   ankit logic
        //logic 11 (ecs nach, insurance) - if the narration begins with "nach-dr" or "nach tran" or "NA CH-DR", then extract the data after "nach-dr" or "nach tran" or "NA CH-DR". Remove "-", numbers, ".", "dt","rej", "tp","ach","a ch" from the extracted data.
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("InsPR")||s.getSecondLevelClassification().equalsIgnoreCase("InsPP")||s.getSecondLevelClassification().equalsIgnoreCase("ECSP")||s.getSecondLevelClassification().equalsIgnoreCase("ECSR"))&& (s.getDescription().startsWith("nach-dr")||s.getDescription().startsWith("nach tran")||s.getDescription().startsWith("NA CH-DR"))).forEach(s->{
            String exKeyWord="";
            if(s.getDescription().contains("nach-dr")) {
                s.setCustomerSupplierName(s.getDescription().substring(s.getDescription().indexOf("nach-dr")).replaceAll("[^A-Za-z ]","").replaceAll("dt","").replaceAll("rej","").replaceAll("tp","").replaceAll("ach","").replaceAll("a ch",""));
            }
            if(s.getDescription().contains("nach tran")) {
                s.setCustomerSupplierName(s.getDescription().substring(s.getDescription().indexOf("nach tran")).replaceAll("[^A-Za-z ]","").replaceAll("dt","").replaceAll("rej","").replaceAll("tp","").replaceAll("ach","").replaceAll("a ch",""));
            }
            if(s.getDescription().contains("NA CH-DR")) {
                s.setCustomerSupplierName(s.getDescription().substring(s.getDescription().indexOf("NA CH-DR")).replaceAll("[^A-Za-z ]","").replaceAll("dt","").replaceAll("rej","").replaceAll("tp","").replaceAll("ach","").replaceAll("a ch",""));
            }
            s.setLogicNO("11");
            System.out.println("logic No. 11   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });


        //logic 12(ecs nach) - if the narration begins with "ecs/" and the number of "/" in the narration is greater than 1 ,
        // then delimit the narration using the "/" as a separater.
        // Once the narration is split in different strings we start from the second string ,
        // if there is no number in the string and the length of the string is greater than 3 ,
        // then extract this else keep checking till the 5th string.If no string satisfies these conditions then give blank.
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("ECSP")||s.getSecondLevelClassification().equalsIgnoreCase("ECSR")) && s.getDescription().startsWith("ecs/")&& (StringUtils.countMatches(s.getDescription(),"/")>1)).forEach(s-> {

            String extKeyword = " ";
            String[] splitString = s.getDescription().split("/" );
            for (int i =1;i<5;i++){
                if(splitString[i].matches("[^0-9]")){
                    s.setCustomerSupplierName(extKeyword);
                    break;
                }
                String number = splitString[i].replaceAll("[^0-9]","");
                String alphabet = splitString[i].replaceAll("[^A-Za-z]","");
                if(number.length()<alphabet.length()){
                    if (splitString[i].length()>3){
                        extKeyword = splitString[i];
                        s.setCustomerSupplierName(extKeyword);
                        break;
                    }
                }
            }
            s.setLogicNO("12");
            System.out.println("logic No. 12   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });







        //logic 13(ecs nach) -f the narration begins with "neft/" or "rtgs/", then extract the data between 2nd and 3rd "/".
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("ECSR")||s.getSecondLevelClassification().equalsIgnoreCase("ECSP")||s.getSecondLevelClassification().equalsIgnoreCase("NP")||s.getSecondLevelClassification().equalsIgnoreCase("NR")||s.getSecondLevelClassification().equalsIgnoreCase("RP")||s.getSecondLevelClassification().equalsIgnoreCase("RR"))&& (s.getDescription().startsWith("neft/")||s.getDescription().startsWith("rtgs/"))).forEach(s-> {
            String exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",2),StringUtils.ordinalIndexOf(s.getDescription(),"/",3));
            s.setCustomerSupplierName(exKeyWord);
            s.setLogicNO("13");
            System.out.println("logic No. 13   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });


        //logic 14(ecs nach, insurance, investment) - if the narration begins with "inb/neft" or "inb/rtgs" or "imps/" or "upi/" or "ift/", then extract the data after 3rd "/". If the extracted data contains "/", then consider the data left of "/". Remove numbers.
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("ECSR")||s.getSecondLevelClassification().equalsIgnoreCase("ECSP")||s.getSecondLevelClassification().equalsIgnoreCase("InsPP")||s.getSecondLevelClassification().equalsIgnoreCase("InsPR")||s.getSecondLevelClassification().equalsIgnoreCase("INVESTP")||s.getSecondLevelClassification().equalsIgnoreCase("INVESTR"))&& (s.getDescription().startsWith("inb/neft")||s.getDescription().startsWith("inb/rtgs")||s.getDescription().startsWith("imps/")||s.getDescription().startsWith("upi/")||s.getDescription().startsWith("ift/"))).forEach(s-> {
            String exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",3));
            if(exKeyWord.contains("/"))
                exKeyWord = s.getDescription().substring(0,s.getDescription().indexOf("/")).replaceAll("[0-9]","");
            s.setCustomerSupplierName(exKeyWord);
            s.setLogicNO("14");
            System.out.println("logic No. 14   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });


        //logic 15(insurance, credit card) - if the narration begins with "brn-", then extract the data after second "-". If the extracted data contains "/", then consider the data left of "/". Remove "chq", "paid to", alphanumeric value, numbers from the extracted data.
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("InsPP")||s.getSecondLevelClassification().equalsIgnoreCase("InsPR")||s.getSecondLevelClassification().equalsIgnoreCase("CCPP")||s.getSecondLevelClassification().equalsIgnoreCase("CCPR"))&& (s.getDescription().startsWith("brn-"))).forEach(s-> {
            String exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"-",2));
            if(exKeyWord.contains("/"))
                exKeyWord = s.getDescription().substring(0,s.getDescription().indexOf("/")).replaceAll("[^A-Za-z]","").replaceAll("chq","").replaceAll("paid to","");
            s.setCustomerSupplierName(exKeyWord);
            s.setLogicNO("15");
            System.out.println("logic No. 15   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });


        //logic 16 (insurance, investment) - if the narration begins with "neft/" or "rtgs/" and the number of "/" in the narration is greater than 1 , then delimit the narration using the "/" as a separater.Once the narration is split in different strings we start from the second string ,if there is no number in the string and the length of the string is greater than 3 , then extract this else keep checking till the 5th string.If no string satisfies these conditions then give blank.
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("InsPP")||s.getSecondLevelClassification().equalsIgnoreCase("InsPR")||s.getSecondLevelClassification().equalsIgnoreCase("INVESTP")||s.getSecondLevelClassification().equalsIgnoreCase("INVESTR"))&& (s.getDescription().startsWith("neft/")||s.getDescription().startsWith("rtgs/")&& (StringUtils.countMatches(s.getDescription(),"/")>1))).forEach(s-> {
            String extKeyword = " ";
            String[] splitString = s.getDescription().split("/" );
            for (int i =1;i<5;i++){
                if(splitString[i].matches("[^0-9]")){
                    s.setCustomerSupplierName(extKeyword);
                    break;
                }
                String number = splitString[i].replaceAll("[^0-9]","");
                String alphabet = splitString[i].replaceAll("[^A-Za-z]","");
                if(number.length()<alphabet.length()){
                    if (splitString[i].length()>3){
                        extKeyword = splitString[i];
                        s.setCustomerSupplierName(extKeyword);
                        break;
                    }
                }
            }
            s.setLogicNO("16");
            System.out.println("logic No. 16   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());

        });





        //logic 17 (insurance, investment) - if the narration begsins with "ecom pur" or "ecs/" or "dd/" or "inb/f", then extract the data between first and second "/".
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("InsPP")||s.getSecondLevelClassification().equalsIgnoreCase("InsPR")||s.getSecondLevelClassification().equalsIgnoreCase("INVESTP")||s.getSecondLevelClassification().equalsIgnoreCase("INVESTR"))&& (s.getDescription().startsWith("ecom pur")||s.getDescription().startsWith("ecs/")||s.getDescription().startsWith("dd/")||s.getDescription().startsWith("inb/f"))).forEach(s-> {
            String exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"-",1),StringUtils.ordinalIndexOf(s.getDescription(),"-",2));
            s.setCustomerSupplierName(exKeyWord);
            s.setLogicNO("17");
            System.out.println("logic No. 17   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());

        });

        //logic 18 (insurance) - if the narration begins with "lic" or "the new" or "the oriental" and contains "/", then extract the data before "/".
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("InsPP")||s.getSecondLevelClassification().equalsIgnoreCase("InsPR"))&& (s.getDescription().startsWith("lic")||s.getDescription().startsWith("the new")||s.getDescription().startsWith("the oriental"))).forEach(s-> {
            if(s.getDescription().contains("/")){
                String  exKeyWord = s.getDescription().substring(0,s.getDescription().indexOf("/"));
                s.setCustomerSupplierName(exKeyWord);
            }
            s.setLogicNO("18");
            System.out.println("logic No. 18   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });

        //logic 19(insurance, investment, credit card) - if the narration contains only alphabets, then extract the whole narration.
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("InsPP")||s.getSecondLevelClassification().equalsIgnoreCase("InsPR")||s.getSecondLevelClassification().equalsIgnoreCase("INVESTP")||s.getSecondLevelClassification().equalsIgnoreCase("INVESTR")||s.getSecondLevelClassification().equalsIgnoreCase("CCPP")||s.getSecondLevelClassification().equalsIgnoreCase("CCPR"))&& (s.getDescription().contains("[A-Za-z]"))).forEach(s-> {
            if(s.getDescription().contains("[A-Za-z]")){
                String  exKeyWord = s.getDescription();
                s.setCustomerSupplierName(exKeyWord);
            }
            s.setLogicNO("19");
            System.out.println("logic No. 19   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });

        //logic 20(investment) - if the narration begins with "brn" and the count of hyphens is less than 3 , then extract the data after 1st "-".
        // If the extracted data contains "-", then consider the data right of "-".
        // and if the extracted data contains "/", then considee the data left of "/". Remove "chq","paid to", numbers ,"trf","from" from the extracted data.
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("INVESTP")||s.getSecondLevelClassification().equalsIgnoreCase("INVESTR")) && s.getDescription().startsWith("brn")&&(StringUtils.countMatches(s.getDescription(),"-")<3)).forEach(s-> {
            String exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"-",1));
            if(exKeyWord.contains("-")) {
                exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(), "-", 1));
            }
            if(exKeyWord.contains("/")) {
                exKeyWord = s.getDescription().substring(0,s.getDescription().indexOf("/")).replaceAll("chq","").replaceAll("paid to","").replaceAll("[0-9]","").replaceAll("trf","").replaceAll("from","");
            }
            s.setCustomerSupplierName(exKeyWord);
            s.setLogicNO("20");
            System.out.println("logic No. 20   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });


        // logic 21(investment) - if the narration begins with "brn" or "mob" and the count of hyphen is 3, then extract the data between first "-" and "/".
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("INVESTP")||s.getSecondLevelClassification().equalsIgnoreCase("INVESTR")) && s.getDescription().startsWith("brn")||s.getDescription().startsWith("mob")&&(StringUtils.countMatches(s.getDescription(),"-")==3)).forEach(s-> {
            String exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"-",1),StringUtils.ordinalIndexOf(s.getDescription(),"-",2));
            s.setCustomerSupplierName(exKeyWord);
            s.setLogicNO("21");
            System.out.println("logic No. 21   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });


        //logic 22(investment) - if the narration begins with "fd" or "td", then extract the first two letters from the narration.
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("INVESTP")||s.getSecondLevelClassification().equalsIgnoreCase("INVESTR")) && s.getDescription().startsWith("fd")||s.getDescription().startsWith("td")).forEach(s-> {
            String exKeyWord = s.getDescription().substring(0,2);
            s.setCustomerSupplierName(exKeyWord);
            s.setLogicNO("22");
            System.out.println("logic No. 22   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });


        // logic 23 logic 14(investment) - If the narration begins with "to", then extract the whole narration. Remove numbers,"to","towards". If the extracted data contains "-", then extract the data before "-".
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("INVESTP")||s.getSecondLevelClassification().equalsIgnoreCase("INVESTR")) && s.getDescription().startsWith("to")).forEach(s-> {
            String exKeyWord = s.getDescription().replaceAll("to","").replaceAll("towards","");
            if(exKeyWord.contains("-")){
                exKeyWord = s.getDescription().substring(0,s.getDescription().indexOf("-"));
                s.setCustomerSupplierName(exKeyWord);
            }
            s.setLogicNO("23");
            System.out.println("logic No. 23   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });

        //logic 24(investment) - if the narration begins with "inb" or "trf" and contains "/" and do not contain "neft","rtgs", then extract the data between 2nd and 3rd "/".
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("INVESTP")||s.getSecondLevelClassification().equalsIgnoreCase("INVESTR")) && s.getDescription().startsWith("inb")||s.getDescription().startsWith("trf")&& s.getDescription().contains("/")).forEach(s-> {
            if(!s.getDescription().contains("neft")){
                String exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",2),StringUtils.ordinalIndexOf(s.getDescription(),"/",3));
                s.setCustomerSupplierName(exKeyWord);
            }
            if(!s.getDescription().contains("rtgs")){
                String exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",2),StringUtils.ordinalIndexOf(s.getDescription(),"-",3));
                s.setCustomerSupplierName(exKeyWord);
            }

            s.setLogicNO("24");
            System.out.println("logic No. 24   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });

        // logic 25(credit card) - if the narration begins with "inb" or "mob". And do not contain "neft" or "rtgs", then extract the data after 2nd "/".
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("CCPP")||s.getSecondLevelClassification().equalsIgnoreCase("CCPR")) && (s.getDescription().startsWith("inb")|| s.getDescription().startsWith("mob"))&& !s.getDescription().contains("neft")&&  !s.getDescription().contains("rtgs")).forEach(s-> {
            String exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",2));
            s.setCustomerSupplierName(exKeyWord);
            s.setLogicNO("25");
            System.out.println("logic No. 25   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());

        });


        //logic 26(loan) - if the narration begins with "neft" or "rtgs/", then extract the data between 2nd and 3rd "/".
        // If the extracted data is alphanumeric value then extract the data after 3rd"/". Remove "disb a","disbursement" from the extracted data.
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("LRPAY")||s.getSecondLevelClassification().equalsIgnoreCase("LoanR")) && s.getDescription().startsWith("neft")|| s.getDescription().contains("rtgs")).forEach(s-> {
            String exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",2),StringUtils.ordinalIndexOf(s.getDescription(),"/",3));
            if(exKeyWord.contains("[^A-Za-z]")) {
                exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",3)).replaceAll("disb a","").replaceAll("disbursement","");
            }
            s.setCustomerSupplierName(exKeyWord);
            s.setLogicNO("26");
            System.out.println("logic No. 26   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });



        // logic 27(loan) - if the narration begins with "inb/neft" or "imps/" or "upi/", then extract the data between 3rd and 4th "/". If the extraction is blank, then extract the data after 4th "/".
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("LRPAY")||s.getSecondLevelClassification().equalsIgnoreCase("LoanR")) && s.getDescription().startsWith("inb/neft")||s.getDescription().startsWith("imps/")&& s.getDescription().startsWith("upi/")).forEach(s-> {
            String exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",3),StringUtils.ordinalIndexOf(s.getDescription(),"/",4));
            if(exKeyWord.isEmpty()||exKeyWord.length()==0) {
                exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",4));
            }
            s.setCustomerSupplierName(exKeyWord);
            s.setLogicNO("27");
            System.out.println("logic No. 27   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });


        // logic 28(loan, tax) - if the narration contains ":disb:" or "chq paid to", then extract the data after ":disb:" or "chq paid to"
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("LRPAY")||s.getSecondLevelClassification().equalsIgnoreCase("LoanR")||s.getSecondLevelClassification().equalsIgnoreCase("Trefund")||s.getSecondLevelClassification().equalsIgnoreCase("TP")) && s.getDescription().contains(":disb:")||s.getDescription().contains("chq paid to")).forEach(s-> {
            if(s.getDescription().contains(":disb:")){
                String exKeyWord = s.getDescription().substring(s.getDescription().indexOf("disb:"));
                s.setCustomerSupplierName(exKeyWord);
            }
            if(s.getDescription().contains("chq paid to")){
                String exKeyWord = s.getDescription().substring(s.getDescription().indexOf("chq paid to"));
                s.setCustomerSupplierName(exKeyWord);
            }
            s.setLogicNO("28");
            System.out.println("logic No. 28   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });


        //logic 29(tax) -  if the narration begins with "neft/" or "rtgs/" and the number of "/" in the narration is greater than 1 , then delimit the narration using the "/" as a separater.
        // Once the narration is split in different strings we start from the second string ,if there is no number in the string and the length of the string is greater than 2 , then extract this else keep checking till the 5th string.
        // If no string satisfies these conditions then give blank.
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("Trefund")||s.getSecondLevelClassification().equalsIgnoreCase("TP")) && s.getDescription().startsWith("neft")|| s.getDescription().contains("rtgs")&&(StringUtils.countMatches(s.getDescription(),"-")>1)).forEach(s-> {
            String extKeyword = " ";
            String[] splitString = s.getDescription().split("/" );
            for (int i =1;i<5;i++){
                if(splitString[i].matches("[^0-9]")){
                    s.setCustomerSupplierName(extKeyword);
                    break;
                }
                String number = splitString[i].replaceAll("[^0-9]","");
                String alphabet = splitString[i].replaceAll("[^A-Za-z]","");
                if(number.length()<alphabet.length()){
                    if (splitString[i].length()>2){
                        extKeyword = splitString[i];
                        s.setCustomerSupplierName(extKeyword);
                        break;
                    }
                }
            }
            s.setLogicNO("29");
            System.out.println("logic No. 29   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });

        //logic 30(tax) - if the narration begins with "inb/" and contains "gst" or "cbdt" or "taxes" or "state tax" or "cbec", then extract the data after 2nd "/".
        // If the extracted data contains "/", then consider the data left of "/". Remove alphanumeric value from the extracted data.
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("Trefund")||s.getSecondLevelClassification().equalsIgnoreCase("TP")) && s.getDescription().startsWith("inb/")&& s.getDescription().contains("gst")||s.getDescription().contains("cbdt")||s.getDescription().contains("taxes")||s.getDescription().contains("cbec")).forEach(s-> {
            String exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",2));
            if(exKeyWord.contains("/")){
                exKeyWord = s.getDescription().substring(0,s.getDescription().indexOf("/"));
                s.setCustomerSupplierName(exKeyWord);
            }
            s.setLogicNO("30");
            System.out.println("logic No. 30   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());
        });


        //logic 31(tax) - if the narration begins with "inb/" and contains "tds", then extract the data after 3rd "/".
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("Trefund")||s.getSecondLevelClassification().equalsIgnoreCase("TP")) && s.getDescription().startsWith("inb/")&& s.getDescription().contains("tds")).forEach(s-> {
            if(s.getDescription().contains("tds")){
                String exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",3));
                s.setCustomerSupplierName(exKeyWord);
            }
            s.setLogicNO("31");
            System.out.println("logic No. 31   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());

        });

        //logic 32(credit card) if the narration begins with "inb" or "mob" and contains "neft", then extract the data after 3rd "/". And if the narration contains "rtgs", then extract the data after 4th "/".
        transactionsList.stream().filter(s->(s.getSecondLevelClassification().equalsIgnoreCase("CCPP")||s.getSecondLevelClassification().equalsIgnoreCase("CCPR")) && (s.getDescription().startsWith("inb")|| s.getDescription().startsWith("mob"))&& !s.getDescription().contains("neft")&&  !s.getDescription().contains("rtgs")).forEach(s-> {
            if (s.getDescription().contains("neft")){
                String exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",3));
                s.setCustomerSupplierName(exKeyWord);
            }
            if (s.getDescription().contains("rtgs")){
                String exKeyWord = s.getDescription().substring(StringUtils.ordinalIndexOf(s.getDescription(),"/",4));
                s.setCustomerSupplierName(exKeyWord);
            }
            s.setLogicNO("32");
            System.out.println("logic No. 32   :-"+s.getDescription()+"   2Lvl  "+s.getSecondLevelClassification());

        });




        return transactionsList;
    }
}
