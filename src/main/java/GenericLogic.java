import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.p4.logics.entity.Transaction;
import com.p4.logics.service.*;
import com.p4.logics.validation.ValidationResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

public class GenericLogic {

    public static void main(String[] args) {

        GenericLogic genericLogic = new GenericLogic();
        JSONParser parser = new JSONParser();
        //Gson gson = new Gson();
        Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
        List<Transaction> transactionsList = new ArrayList<>();
        JSONArray jsonTrnxArray = null;
        try {
            jsonTrnxArray = (JSONArray) parser.parse(new FileReader("/home/test-4/Downloads/7dfc1168-b971-4334-83bc-4fbd379fe4fe.json"));
            transactionsList = (List<Transaction>) jsonTrnxArray.stream().map(i -> gson.fromJson(i.toString(), Transaction.class)).collect(Collectors.toList());
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        String bankCode ="FI00002";
//        transactionsList.stream().forEach(s-> System.out.println(s.getDescription()));
        List<Transaction>  result= genericLogic.Procedure(transactionsList,bankCode);
           if(result!=null) {
               genericLogic.ExcelConversion(result);
           }
    }

    public void ExcelConversion(List<Transaction> transactionsList){
        //reWrite to excel
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("BankStatement");
        File convExcel = new File("/home/test-4/Downloads/sample/sample.xlsx");
        Row row;
        Cell cell;
        int rowIndex = 1;
//        String[] headers = {"id","updatedDate","fileId","txnId","reference_id","date","valueDate","description","chequeReferenceNo","transactionType","creditAmount","debitAmount","balanceAmount","firstLevelClassification","secondLevelClassification","thirdLevelClassification","clientName","chequeNo","fiName","balanceError","dateError","tag","checkReturnType","createdDate","probableSalary","customerSupplierName","Logic NO."};
          String[] headers = {"txnId","reference_id","description","creditAmount","debitAmount","balanceAmount","firstLevelClassification","secondLevelClassification","thirdLevelClassification","customerSupplierName","Logic NO."};
                Row headerrow = sheet.createRow(0);
        for (int iToken = 0; iToken < headers.length; iToken++) {
            cell = headerrow.createCell(iToken);
            cell.setCellValue(headers[iToken]);
        }

        for (int i = 0; i < transactionsList.size()-1; i++) {
            row = sheet.createRow(rowIndex);
            row.createCell(0).setCellValue(transactionsList.get(i).getTxnId());
            row.createCell(1).setCellValue(transactionsList.get(i).getReference_id());
            row.createCell(2).setCellValue(transactionsList.get(i).getDescription());
            row.createCell(3).setCellValue(transactionsList.get(i).getCreditAmount().toString());
            row.createCell(4).setCellValue(transactionsList.get(i).getDebitAmount().toString());
            row.createCell(5).setCellValue(transactionsList.get(i).getBalanceAmount().toString());
            String fLvlc1 = getFullKeyWord(transactionsList.get(i).getFirstLevelClassification());
            row.createCell(6).setCellValue(fLvlc1);
            String sLvlc2 = getFullKeyWord(transactionsList.get(i).getSecondLevelClassification());
            row.createCell(7).setCellValue(sLvlc2);
            row.createCell(8).setCellValue(transactionsList.get(i).getThirdLevelClassification());
            row.createCell(9).setCellValue(transactionsList.get(i).getCustomerSupplierName());
            row.createCell(10).setCellValue(transactionsList.get(i).getLogicNO());
            rowIndex ++ ;
        }
        try {
            FileOutputStream outputStream = new FileOutputStream(convExcel);
            workbook.write(outputStream);
            workbook.close();
        }catch (Exception e){
            e.printStackTrace();

        }
    }

    public List<Transaction>  Procedure(List<Transaction> transactionList, String bank_code) {
        List<Transaction>  result = null;
        switch (bank_code) {
            case "FI00002":
                FI00002_allahabadBank albd = new FI00002_allahabadBank();
                result = albd.fI00002_allahabad_Bank(transactionList);
                return result;
            case "FI00003":
                FI00003_andhraBank andhraBank =new FI00003_andhraBank();
                result = andhraBank.fi00003_andhraBank(transactionList);
                return result;
            case "FI00004":
                FI00004_AUSmallFinanceBank au = new FI00004_AUSmallFinanceBank();
                result = au.fI00004_AUSmallFinanceBank(transactionList);
                return result;

            case "FI00005":
                FI00005_axisBank axis = new FI00005_axisBank();
                result = axis.fi00005_axisBank(transactionList);
                return result;
            case "FI00088":
                FI00088_abhyudayaCooperative_Bank ACBL = new FI00088_abhyudayaCooperative_Bank();
                result = ACBL.fI00088_abhyudayaCooperative_Bank(transactionList);
                return result;
            default:
                System.out.println("no match");
        }
        return result;
    }


    public String getFullKeyWord(String key){
        Map<String, String> classificationmap = new HashMap<String, String>();
        classificationmap.put("CONTRAP","Contra");
        classificationmap.put("CONTRAR","Contra");
        classificationmap.put("BCP","Bank Charges");
        classificationmap.put("BCR","Bank Charges");
        classificationmap.put("IOR","Inward Online Return");
        classificationmap.put("OOR","Outward Online Return");
        classificationmap.put("OCR","Outward Cheque Return");
        classificationmap.put("ICReturn","Inward Cheque Return");
        classificationmap.put("ERP","ECS/NACH Return");
        classificationmap.put("ERR","ECS/NACH Return");
        classificationmap.put("CASHRP","Cash Return");
        classificationmap.put("CASHRR","Cash Return");
        classificationmap.put("UP","Utility Payment");
        classificationmap.put("BIP","Bank Instrument");
        classificationmap.put("BIR","Bank Instrument");
        classificationmap.put("InsPP","Insurance");
        classificationmap.put("InsPR","Insurance");
        classificationmap.put("IntP","Interest Paid");
        classificationmap.put("IntR","Interest Received");
        classificationmap.put("SO","Sweep Out");
        classificationmap.put("SI","Sweep In");
        classificationmap.put("INVESTP","Investment");
        classificationmap.put("INVESTR","Investment");
        classificationmap.put("CCPP","Card Transaction");
        classificationmap.put("CCPR","Card Transaction");
        classificationmap.put("RENTP","Rent");
        classificationmap.put("RENTR","Rent");
        classificationmap.put("WCP","Working Capital");
        classificationmap.put("WCR","Working Capital");
        classificationmap.put("LRPAY","Loan");
        classificationmap.put("LoanR","Loan");
        classificationmap.put("Trefund","Tax");
        classificationmap.put("TP","Tax");
        classificationmap.put("CE","Company Expense");
        classificationmap.put("SPP","Salary");
        classificationmap.put("SPR","Salary");
        classificationmap.put("FOREXP","Forex");
        classificationmap.put("FOREXR","Forex");
        classificationmap.put("REFUNDP","Refund/Reversal");
        classificationmap.put("REFUNDR","Refund/Reversal");
        classificationmap.put("ECSP","ECS/NACH Payment");
        classificationmap.put("ECSR","ECS/NACH Receipt");
        classificationmap.put("RP","RTGS Payment");
        classificationmap.put("RR","RTGS Receipt");
        classificationmap.put("NP","NEFT Payment");
        classificationmap.put("NR","NEFT Receipt");
        classificationmap.put("IP","IMPS Payment");
        classificationmap.put("IR","IMPS Receipt");
        classificationmap.put("TranFIP","FI Transaction");
        classificationmap.put("TranFIR","FI Transaction");
        classificationmap.put("CD","Cash Deposit");
        classificationmap.put("CWD","Cash Withdrawal");
        classificationmap.put("CP","Cheque Payment");
        classificationmap.put("CR","Cheque Receipt");
        classificationmap.put("OP","Online Payment");
        classificationmap.put("OR","Online Receipt");
        classificationmap.put("OPay","Other Payment");
        classificationmap.put("ORec","Other Receipt");
        classificationmap.put("IHPay","Inhouse Payment");
        classificationmap.put("IHRec","Inhouse Receipt");
        return classificationmap.get(key);
    }



}
